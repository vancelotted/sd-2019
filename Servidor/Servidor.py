#coding: utf-8
import socket 
import select 
import sys 
from thread import *


# Configurações de conexão 
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
enderecoIP = 0 
porta = 0
numConexao = 0

# Lista de usuarios conectados no chat
list_of_clients = [] 
listaUsuarios = {}

# Mensagens defauts para especificas situações
mensagemSis = {"conectado":"[{0}] entrou no chat.",
               "desconectado":"[{0}] saiu do chat.",
               "mensagem" : "[{0}]: {1}",
               "paramincorreto":"[ERRO] - Parametros incorretos. Forma correta: <script> <IP address> <port number> <numeroDeConexaoSuportada>"
              }


# Thread para um novo cliente
def clientthread(conexao, addr): 
  
    global mensagemSis  
    conexao.send("Conectado")
    
    while True:
        
        apelidoUsuario = conexao.recv(2048)
        
        if(apelidoValido(apelidoUsuario)):
            
            conexao.send("Aceito")
            listaUsuarios[apelidoUsuario] = conexao
            if(len(list_of_clients)>1):
                mensagemRetorno = mensagemSis["conectado"].format(apelidoUsuario)
                broadcast(mensagemRetorno, conexao)
            
            break
        
        else:
            conexao.send("Recusado")
    
    
    
    
    while True: 
            try: 
                mensagem = conexao.recv(2048) 
                if mensagem: 
  
                    mensagem_to_send = mensagemSis["mensagem"].format(apelidoUsuario,mensagem) 
                    broadcast(mensagem_to_send, conexao)

                else: 
                    remove(apelidoUsuario) 
  
            except: 
                continue
  
  
  
  
# Essa função é responsável por transmitir a mensagem a todos, exceto quem enviou a mensagem 
def broadcast(mensagem, conexaoection): 
    for clients in list_of_clients: 
        if clients!=conexaoection: 
            try: 
                clients.send(mensagem) 
            except: 
                clients.close() 
  
                remove(clients) 

# Remove a conexão do cliente do qual não está mais connectado                
def remove(conexao): 
    
    apelido = getApelido(conexao)
    if apelido != '':
        listaUsuarios.pop(apelido)
        mensagemRetorno = mensagemSis["desconectado"].format(apelido)
        broadcast(mensagemRetorno, conexao)    
    


# Funcao responsavel por carregar os parametros para inicializar o servidor
def inicializarServidor():
    
    carregarParametros()  
    server.bind((enderecoIP, porta)) 
    server.listen(numConexao) 


# Função responsavel por verificar se o apelido/login já existe
def apelidoValido(apelido):
    
    if apelido in listaUsuarios.keys():
        return False
    else: 
        return True

def getApelido(conexao):
    
    for apelido in listaUsuarios:
        
        if(conexao == listaUsuarios[apelido]):
            return apelido

    return ""
    
# Funcao responsavel por validar os parametros enviados via linha de comando e carrega-los
def carregarParametros():
    
    try:
        if len(sys.argv) != 4:
            raise Exception
        
        global enderecoIP
        global porta
        global numConexao
        
        enderecoIP = str(sys.argv[1])
        porta = int(sys.argv[2])
        numConexao = int(sys.argv[3])
        
    except:
        print (mensagemSis["paramincorreto"])
        exit()



# O servidor aguarda conexão de novos clientes
def aguardarConexoes():
  
    print  ("Servidor Inicializado")
    
    while True: 
      
        conexao, clienteIP = server.accept() 
        list_of_clients.append(conexao) 
        print (clienteIP[0] + " Conectado")
        start_new_thread(clientthread,(conexao,clienteIP))     
      
    conexao.close() 
    server.close()
    
inicializarServidor()
aguardarConexoes()
