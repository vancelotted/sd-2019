#coding: utf-8
import socket 
import select 
import sys
import datetime
from termios import tcflush, TCIFLUSH
import time


# Configurações de conexão 
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
enderecoIP = 0 
porta = 0
timeout = 30

# Apelido utilizado pelo participante do chat
apelido = ""

# Saudação Inicial
menu = """
******************************************************* \n
*       COMANDOS CHAT COMPUTACAO UERJ                 * \n
******************************************************* \n
*  \\ajuda      |Para exibir o menu                     * \n
*  \sair       |Para sair do chat                      * \n
******************************************************* \n"""



# Cada usuario necessita de um login/apelido para entrar no chat.
# O login/apelido é único para cada participante
# Essa funcao é reponsável por auxiliar o usuario a criar um login.
def login():
    
    try:
        sockets_list = [sys.stdin, server] 
        read_sockets,write_socket, error_socket = select.select(sockets_list,[],[]) 
        loginNaoAceito = True
        
        
        while loginNaoAceito:
            
            for socks in read_sockets: 
                
                if socks == server:
                    
                    message = socks.recv(2048)
                    
                    if message == "Aceito":
                        
                        print ("\n\n **** \o/ \o/ \o/ Seja bem vindo ao chat UERJ - Computação \o/ \o/ \o/ **** \n\n" + menu).decode("utf-8")
                        loginNaoAceito = False
                    
                    else:
                
                        if message == "Recusado":
                
                            print ("Apelido já utilizado.").decode("utf-8")
        
                        informarApelido()
    
    except socket.error:
    
        print("[INFO] - Serviço indisponível. Tente mais tarde.")
        sys.exit()

    except:
        print(("[ERRO] - {0}").format(sys.exc_info()[0]))
        sys.exit()



# Funcao auxiliar para a funcao login        
def informarApelido():
    
    global apelido  
    apelido = raw_input("Informe o seu apelido:")
    server.send(apelido)
    


# Funcao responsavel por validar os parametros enviados via linha de comando e carrega-los
def carregarParametros():
    
    try:
        if len(sys.argv) != 3:
            raise Exception
        
        global enderecoIP
        global porta
        
        enderecoIP =str(sys.argv[1])
        porta = int(sys.argv[2])
        
    except:
        print ("[ERRO] - Parametros incorretos. Forma correta: <script> <IP address> <port number> <numeroDeConexaoSuportada>")
        sys.exit()

# Funcao responsavel por connectar o cliente ao servidor
def conectar():
    try:
        server.connect((enderecoIP, porta))
    except:
        print "[INFO] - Serviço indisponível. Tente mais tarde."
        sys.exit()


# Apos passar o estagio de "login", a aplicação fica aguarado uma mensagem a ser recebida do servidor ou o usuário enviar uma mensagem ao grupo
def conectadoNoChat():
    
    conectado = True
    
    while conectado:
        
      
        try:
            
            sockets_list = [sys.stdin, server]
            read_sockets,write_socket, error_socket = select.select(sockets_list,[],[]) 
          
    
            for socks in read_sockets: 
                
                if socks == server: 
                    
                    message = socks.recv(2048)
                    
                    # Tratamento caso o servidor fique indisponível
                    if message == "":
                        
                        time.sleep(timeout)
                        message = socks.recv(2048)
                        
                        if message == "":
                        
                            print("[INFO] - O chat ficou indisponível :( Tente mais tarde.")
                            sys.exit()
                    
                    
                    print ("{0} - {1}").format(getHora(),message) 
                    
                else: 
                    message = sys.stdin.readline()
                    tcflush(sys.stdin, TCIFLUSH)
                    
                    sys.stdout.write(("{0} - [Você]:  {1}").format(getHora(),message))
                    sys.stdout.flush()
                    
                    if(message.find("\sair")>=0):
                        conectado = False
                    else:
                        
                        server.send(message)
                        
        except socket.error:
            print("[INFO] - O chat ficou indisponível :( Tente mais tarde.")
            sys.exit()
        except:
            print(("[ERRO] - {0}").format(sys.exc_info()[0]))
            sys.exit()     
                    
    server.close() 

def getHora():
    
    data_e_hora_atuais = datetime.datetime.now()
    data_e_hora_em_texto = data_e_hora_atuais.strftime("%d/%m/%Y %H:%M:%S")
    
    return ("[{0}]").format(data_e_hora_em_texto)


# Função principal, abaixo temos os passos de como a aplicação funciona.    
def iniciarlizarCliente():
    
    carregarParametros()
    conectar()
    login()
    conectadoNoChat()

iniciarlizarCliente()